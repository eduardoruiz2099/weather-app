package com.eduardo.weatherapp

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.util.TypedValue.COMPLEX_UNIT_SP
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentTransaction
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.eduardo.weatherapp.UI.CurrentWeatherFragment
import com.google.android.material.bottomnavigation.BottomNavigationView


class MainActivity : AppCompatActivity() {

    private lateinit var bottomNavigationView: BottomNavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setNavController()
        requestLocationPermission()
    }

    private fun setNavController() {
        bottomNavigationView = findViewById(R.id.bottomNavigationView)
        val navigationView = findNavController(R.id.fragmentContainerView)

        bottomNavigationView.setupWithNavController(navigationView)
    }

    private fun hasLocationPermission() =
        ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED

    private fun requestLocationPermission() {
        if(!hasLocationPermission()) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 0)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == 0 && grantResults.isNotEmpty()) {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                supportFragmentManager.beginTransaction().replace(R.id.currentWeatherFragment, CurrentWeatherFragment()).commit()
            }
            else {
                setNoLocationState()
            }
        }
    }

    private fun setNoLocationState() {
        bottomNavigationView.menu.findItem(R.id.forecastFragment_navigation).isEnabled = false
        findViewById<ProgressBar>(R.id.progressBar).visibility = View.GONE
        findViewById<CardView>(R.id.cardView).visibility = View.GONE
        findViewById<TextView>(R.id.current_temperature_textview).text = getString(R.string.no_location_text)
        findViewById<TextView>(R.id.current_temperature_textview).setTextSize(COMPLEX_UNIT_SP,24F)
        findViewById<ImageView>(R.id.current_icon_image).setImageResource(R.drawable.scattered_clouds)
    }
}