package com.eduardo.weatherapp.OpenWeatherApi

import retrofit2.Call

class OpenWeather(latitude: Double, longitude: Double) {

    private var openWeatherInstance: OpenWeatherInstance = OpenWeatherInstance()

    private val apiKey: String = "8459f4aa56c3911e1e2c9cda89f76c16"
    private var currentLatitude: Double = latitude
    private var currentLongitude: Double = longitude

    fun callOpenWeatherApi(): Call<WeatherDataModel> {
        return openWeatherInstance.service.getCurrentWeatherData(currentLatitude.toString(), currentLongitude.toString(), apiKey)
    }

}