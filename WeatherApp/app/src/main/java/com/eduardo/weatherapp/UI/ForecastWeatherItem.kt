package com.eduardo.weatherapp.UI

data class ForecastWeatherItem(
    val day: String,
    val weather: String,
    val temperature: String,
    val description: String,
    val icon: String
)