package com.eduardo.weatherapp.OpenWeatherApi

class WeatherDataModel {
    var current: Current? = null
    var daily = ArrayList<Daily>()
}

class Current {
    var temp: Float = 0.0F
    var feels_like: Float = 0.0F
    var humidity: Int = 0
    var wind_speed: Float = 0.0F
    var weather = ArrayList<Weather>()
}

class Daily {
    var temp: Temp? = null
    var weather = ArrayList<Weather>()
}

class Weather {
    var main: String? = null
    var description: String? = null
    var icon: String? = null
}

class Temp {
    var day: Float = 0.0F
}