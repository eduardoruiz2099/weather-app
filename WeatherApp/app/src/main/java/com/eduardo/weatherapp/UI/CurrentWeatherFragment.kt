package com.eduardo.weatherapp.UI

import android.Manifest
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.eduardo.weatherapp.OpenWeatherApi.OpenWeather
import com.eduardo.weatherapp.OpenWeatherApi.WeatherDataModel
import com.eduardo.weatherapp.R
import com.google.android.gms.location.LocationServices
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

class CurrentWeatherFragment() : Fragment() {

    private lateinit var openWeather: OpenWeather
    private lateinit var progressBar: ProgressBar

    private lateinit var currentWeatherTextView: TextView
    private lateinit var currentTemperatureTextView: TextView
    private lateinit var currentDescriptionTextView: TextView
    private lateinit var currentIconImageView: ImageView
    private lateinit var currentLocation: TextView
    private lateinit var currentDayTextView: TextView
    private lateinit var currentWindTextView: TextView
    private lateinit var currentHumidityTextView: TextView
    private lateinit var currentFeelingTextView: TextView

    private val dateFormat = SimpleDateFormat("EEEE, dd MMM")
    private val calendar: Calendar = GregorianCalendar()

    override fun onCreateView( inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view =  inflater.inflate(R.layout.fragment_current_weather, container, false)

        currentWeatherTextView = view.findViewById(R.id.current_weather_textview)
        currentTemperatureTextView = view.findViewById(R.id.current_temperature_textview)
        currentDescriptionTextView = view.findViewById(R.id.current_weather_description_textview)
        currentIconImageView = view.findViewById(R.id.current_icon_image)
        currentLocation = view.findViewById(R.id.current_location_textview)
        currentDayTextView = view.findViewById(R.id.current_day_textview)
        currentWindTextView = view.findViewById(R.id.current_wind_textview)
        currentHumidityTextView = view.findViewById(R.id.current_humidity_textview)
        currentFeelingTextView = view.findViewById(R.id.current_feeling_textview)
        progressBar = view.findViewById(R.id.progressBar)

        getLastLocation()

        return view
    }

    private fun getLastLocation() {
        if(ContextCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            val fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())

            fusedLocationClient.lastLocation.addOnSuccessListener {
                    location : Location? ->
                        location?.let {
                            openWeather = OpenWeather(it.latitude, it.longitude)
                            setCurrentCityLocation(it.latitude, it.longitude)
                            setCurrentDay()
                            getOpenWeatherData()
                        }
            }
        }
    }

    private fun getOpenWeatherData() {
        val apiResponse = openWeather.callOpenWeatherApi()

        apiResponse.enqueue( object : Callback<WeatherDataModel> {
            override fun onResponse(call: Call<WeatherDataModel>?, response: Response<WeatherDataModel>) {
                if(response.code() == 200) {
                    setCurrentWeatherData(response.body()!!)
                    progressBar.visibility = View.GONE
                }
            }

            override fun onFailure(call: Call<WeatherDataModel>?, t: Throwable?) {
                Log.d("DEBUG", t.toString())
            }
        })
    }

    private fun setCurrentDay() {
        currentDayTextView.text = dateFormat.format(calendar.time)
    }

    private fun setCurrentCityLocation(currentLatitude: Double, currentLongitude: Double) {
        val geocoder = Geocoder(requireActivity(), Locale.getDefault())
        val addresses: List<Address> = geocoder.getFromLocation(currentLatitude, currentLongitude, 1)
        val cityName: String = addresses[0].locality
        currentLocation.text = cityName
    }

    private fun setCurrentWeatherData(currentResponse: WeatherDataModel) {
        currentWeatherTextView.text = currentResponse.current!!.weather[0].main.toString()
        currentTemperatureTextView.text = "${currentResponse.current!!.temp.toInt()}°"
        currentDescriptionTextView.text = currentResponse.current!!.weather[0].description.toString()
        currentWindTextView.text = "${currentResponse.current!!.wind_speed}mph"
        currentHumidityTextView.text = "${currentResponse.current!!.humidity}%"
        currentFeelingTextView.text = "${currentResponse.current!!.feels_like.toInt()}°C"

        currentIconImageView.setImageResource(when(currentResponse.current!!.weather[0].icon.toString()){
            "01d", "01n" -> R.drawable.clear_sky
            "02d", "02n" -> R.drawable.few_clouds
            "03d", "03n" -> R.drawable.scattered_clouds
            "04d", "04n" -> R.drawable.scattered_clouds
            "09d", "09n" -> R.drawable.shower_rain
            "10d", "10n" -> R.drawable.rain
            "11d", "11n" -> R.drawable.thunderstorm
            "13d", "13n" -> R.drawable.snow
            "50d", "50n" -> R.drawable.mist
            else  -> R.drawable.backgroud_box_drawable
        })
    }

}