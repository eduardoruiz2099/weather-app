package com.eduardo.weatherapp.OpenWeatherApi

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class OpenWeatherInstance {
    val retrofit = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl("https://api.openweathermap.org/")
        .build()

    val service = retrofit.create(OpenWeatherRequests::class.java)
}