package com.eduardo.weatherapp.UI

import android.Manifest
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eduardo.weatherapp.OpenWeatherApi.OpenWeather
import com.eduardo.weatherapp.OpenWeatherApi.WeatherDataModel
import com.eduardo.weatherapp.R
import com.google.android.gms.location.LocationServices
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

class ForecastFragment : Fragment() {

    private lateinit var openWeather: OpenWeather
    private lateinit var progressBar: ProgressBar
    private lateinit var forecastLocation: TextView
    private lateinit var recyclerView: RecyclerView

    private val dateFormat = SimpleDateFormat("EEEE")

    override fun onCreateView( inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle? ): View? {
        val view =  inflater.inflate(R.layout.fragment_forecast, container, false)

        forecastLocation = view.findViewById(R.id.forecast_location_textview)
        progressBar = view.findViewById(R.id.progressBar)
        recyclerView = view.findViewById(R.id.weather_items_recyclerview)

        getLastLocation()

        return view
    }

    private fun getLastLocation() {
        if(ContextCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            val fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())

            fusedLocationClient.lastLocation.addOnSuccessListener {
                    location : Location? ->
                        location?.let {
                            openWeather = OpenWeather(it.latitude, it.longitude)
                            setCurrentCityLocation(it.latitude, it.longitude)
                            getOpenWeatherData()
                        }
            }
        }
    }

    private fun getOpenWeatherData() {
        val apiResponse = openWeather.callOpenWeatherApi()

        apiResponse.enqueue( object : Callback<WeatherDataModel> {
            override fun onResponse(call: Call<WeatherDataModel>?, response: Response<WeatherDataModel>) {
                if(response.code() == 200) {
                    setForecastData(response.body()!!)
                    progressBar.visibility = View.GONE
                }
            }

            override fun onFailure(call: Call<WeatherDataModel>?, t: Throwable?) {
                Log.d("DEBUG", t.toString())
            }
        })
    }

    private fun setCurrentCityLocation(currentLatitude: Double, currentLongitude: Double) {
        val geocoder = Geocoder(requireActivity(), Locale.getDefault())
        val addresses: List<Address> = geocoder.getFromLocation(currentLatitude, currentLongitude, 1)
        val cityName: String = addresses[0].locality
        forecastLocation.text = cityName
    }

    private fun setForecastData(currentResponse: WeatherDataModel) {
        val forecastDay = mutableListOf<String>()
        val forecastWeather = mutableListOf<String>()
        val forecastTemperature = mutableListOf<String>()
        val forecastDescription = mutableListOf<String>()
        val forecastIcon = mutableListOf<String>()

        // Get next 7 days from the current day
        for (i in 1..7) {
            val calendar: Calendar = GregorianCalendar()
            calendar.add(Calendar.DATE, i)
            forecastDay.add(dateFormat.format(calendar.time))
        }

        // Get data for each forecast items
        for(day in 0..6) {
            forecastWeather.add(currentResponse.daily[day].weather[0].main!!.toString())
            forecastTemperature.add(currentResponse.daily[day].temp!!.day.toInt().toString())
            forecastDescription.add(currentResponse.daily[day].weather[0].description!!.toString())
            forecastIcon.add(currentResponse.daily[day].weather[0].icon.toString())
        }

        val weatherItems: MutableList<ForecastWeatherItem> = mutableListOf()

        // Set the information for each forecast item
        for(day in 0..6) {
            weatherItems.add(ForecastWeatherItem(
                forecastDay[day],
                forecastWeather[day],
                forecastTemperature[day],
                forecastDescription[day],
                forecastIcon[day]
            ))
        }

        val weatherItemsRecyclerView = recyclerView
        val adapter = ForecastAdapter(weatherItems)

        weatherItemsRecyclerView.adapter = adapter
        weatherItemsRecyclerView.layoutManager = LinearLayoutManager(activity)
    }

}