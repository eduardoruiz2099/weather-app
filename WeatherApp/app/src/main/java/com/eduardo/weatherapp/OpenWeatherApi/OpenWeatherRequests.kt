package com.eduardo.weatherapp.OpenWeatherApi

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface OpenWeatherRequests {
    @GET("data/2.5/onecall?units=metric")
    fun getCurrentWeatherData(@Query("lat") lat: String, @Query("lon") lon: String, @Query("appid") appid: String): Call<WeatherDataModel>
}