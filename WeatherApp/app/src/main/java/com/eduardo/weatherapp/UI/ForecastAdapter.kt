package com.eduardo.weatherapp.UI

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eduardo.weatherapp.R

class ForecastAdapter(var weatherDays: List<ForecastWeatherItem>) : RecyclerView.Adapter<ForecastAdapter.ForecastViewHolder>() {
    inner class ForecastViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ForecastViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.forecast_weather_item, parent, false)
        return ForecastViewHolder(view)
    }

    override fun onBindViewHolder(holder: ForecastViewHolder, position: Int) {
        holder.itemView.apply {
            val day = weatherDays[position].day
            val weather = weatherDays[position].weather
            val temperature = weatherDays[position].temperature
            val description = weatherDays[position].description
            val icon = when(weatherDays[position].icon) {
                "01d", "01n" -> R.drawable.clear_sky
                "02d", "02n" -> R.drawable.few_clouds
                "03d", "03n" -> R.drawable.scattered_clouds
                "04d", "04n" -> R.drawable.scattered_clouds
                "09d", "09n" -> R.drawable.shower_rain
                "10d", "10n" -> R.drawable.rain
                "11d", "11n" -> R.drawable.thunderstorm
                "13d", "13n" -> R.drawable.snow
                "50d", "50n" -> R.drawable.mist
                else -> R.drawable.backgroud_box_drawable
            }

            findViewById<TextView>(R.id.day_textview).text = day
            findViewById<TextView>(R.id.forecast_weather_textview).text = weather
            findViewById<TextView>(R.id.forecast_temperature_textview).text = "${temperature}°"
            findViewById<TextView>(R.id.forecast_description_textview).text = description
            findViewById<ImageView>(R.id.forecast_day_weather_image).setImageResource(icon)
        }
    }

    override fun getItemCount(): Int {
        return weatherDays.size
    }
}